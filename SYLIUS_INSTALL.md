Documentation
-------------

Documentation is available at [docs.sylius.org](http://docs.sylius.org).


# Requirements for silius

* OS - Ubuntu (or Debian)
* GIT
* NGINX
* PHP 7.2
* MYSQL
* YARN

# 1. Installation
------------

## 1.1. Clone project
Clone Sylius project:
```bash
mkdir /var/www/foodlabs.co.il
cd /var/www/foodlabs.co.il

git clone git@nix.githost.io:sylius-amir-multivendor-platform/sylius-amir-multivendor-platform.git ./
git checkout develop
git pull origin develop
composer install

```
Make configs:
```bash
cp app/config/parameters.yml.dist app/config/parameters.yml
cp app/config/parameters_email.yml.dist  app/config/parameters_email.yml
cp app/config/parameters_taxation.yml.dist app/config/parameters_taxation.y
```
Notes: In the future, you will need to change the configuration storage structure!

Write parameters for smtp account in file __parameters_email.yml__

## 1.2. Setup database

Make database on mysql:
```bach
mysql -u root -p
CREATE DATABASE sylius_sample COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON sylius_sample.* TO sylius_sample@localhost IDENTIFIED BY 'sylius_sample_password';
```

Edit file __app/config/parameters.yml__. Write parameters: __database name, user, password__
```
parameters:
    database_driver: pdo_mysql
    database_host: mysql
    database_port: 3306
    database_name: sylius_sample
    database_user: sylius_sample
    database_password: sylius_sample_password

    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: ~
    mailer_password: ~

    secret: EDITME
```
## 1.3. Restore database
```bash
mysql -u db_user -p
show databases;
use sylius_sample;
source /var/www/_temp/dump-20180808-183820.sql
exit
```

## 1.4 RESTORE MEDIA

Copy files from media arhive:
``` bash
ls -la web/media/
```
* cache
* image


## 1.4. Compile Stiles & JavaScripts
```bash
yarn install
yarn run gulp
```


## 1.5. Addons (CKEditor)
[How to enable WYSIWYG editor in almost any place in Sylius within 5 minutes](https://github.com/FriendsOfSylius/SyliusGoose/blob/master/StepByStep/WYSIWYG_EDITOR_IN_ANY_FORM.md)

```bach
bin/console ckeditor:install
```
```bach
bin/console assets:install
```
Copy folders for theme
```bach
cp web/bundles/bitbagsyliuscmsplugin web/bundles/_themes/foodlabs/custom-theme/bitbagsyliuscmsplugin
cp web/bundles/ivoryckeditor web/bundles/_themes/foodlabs/custom-theme/ivoryckeditor
```

# 2. Update from Git-repository
```
cd /var/www/foodlabs.co.il

git checkout develop
git pull origin develop
composer install
php bin/console doctrine:migrations:migrate
yarn install
yarn run gulp
chown -R www-data:www-data /var/www/foodlabs.co.il
chmod -R 775 /var/www/foodlabs.co.il
rm -R /var/www/foodlabs.co.il/var/cache/prod
rm -R /var/www/foodlabs.co.il/var/cache/prod
```
