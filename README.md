# 1. INSTALL OLD DATA

Clone Docker project:
```bash
git clone git@bitbucket.org:corvet/sylius_amir_docker.git ./
mkdir sylius_project
```

Clone Sylius project:
```bash
cd sylius_project/
git clone git@nix.githost.io:sylius-amir-multivendor-platform/sylius-amir-multivendor-platform.git ./
git checkout develop
git pull origin develop
```

Make configs:
```bash
cp app/config/parameters.yml.dist app/config/parameters.yml
cp app/config/parameters_email.yml.dist  app/config/parameters_email.yml
cp app/config/parameters_taxation.yml.dist app/config/parameters_taxation.yml
```

Notes: In the future, you will need to change the configuration storage structure!

Edit file __app/config/parameters.yml__
```bash
parameters:
    database_driver: pdo_mysql
    database_host: mysql
    database_port: 3306
    database_name: db_name
    database_user: db_user
    database_password: db_password

    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: ~
    mailer_password: ~

    secret: EDITME
```
## 1.1 START DOCKER

Build docker:
```bash
cd ./../docker_symfony/
docker-compose ps
docker-compose down
docker-compose up -d --build
```

## 1.2 SETUP SYLIUS

```bash
sudo chmod -R 777 ./../sylius_project/
docker-compose ps
docker exec -it s3sylius_php_fpm bash
ls -la
composer install
exit
```

## 1.3 RESTORE DATABASE

Copy backup into container:
```bash
mkdir ./../sylius_project/var/docker/mysql_data/db/_temp
cp FILE_BACKUP ./../sylius_project/var/docker/mysql_data/db/_temp/dump-20180808-183820.sql
tail ./../sylius_project/var/docker/mysql_data/db/_temp/dump-20180808-183820.sql
docker-compose ps
docker exec -it s3sylius_mysql bash
mysql -u db_user -p
```
Enter password: db_password
```bash
show databases;
use db_name;
source /var/lib/mysql/_temp/dump-20180808-183820.sql
exit
exit
```

## 1.4 RESTORE MEDIA

Copy files from media arhive:
```bash
ls -la ./../sylius_project/web/media/
```
- cache
- image

## 1.5 RUN APPLICATION

```bash
docker-compose ps
      Name                    Command              State           Ports
---------------------------------------------------------------------------------
s3sylius_mysql     docker-entrypoint.sh mysqld     Up      0.0.0.0:3371->3306/tcp
s3sylius_nginx     nginx -g daemon off;            Up      0.0.0.0:7171->80/tcp
s3sylius_php_fpm   docker-php-entrypoint php-fpm   Up      9000/tcp
```

Open on brauser: http://0.0.0.0:7171/app_dev.php

## 1.6 RUN YARN GULP

```bash
docker-compose ps
docker exec -it s3sylius_php_fpm bash
yarn install
yarn run gulp
Ctrl+C
exit
```

## 1.7 PROBLEMS WITH ACCESS

```bash
sudo chmod -R 777 ./../sylius_project/
```

## 1.8 ADMIN PANEL

http://0.0.0.0:7171/app_dev.php/admin/login

- login:login20180101@gmail.com
- password:**********
